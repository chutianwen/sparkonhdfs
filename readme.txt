=========================================================
ReadMe
Author:Tianwen Chu
Date 10/6/2016
=========================================================

Introduction
------------
This repo has several spark projects. 



Projects
--------
1). GraphApp: 1000Genome graph model project
2.) HDFSBenchMark:is to research performance of spark standalone
mode running on HDFS. 

BuildFile
---------
build.sbt contains build configuations for all the projects

sbt clean compile
sbt package


Configuration
-------------
KVM 16 sockets/nodes cluster
HDFS
Spark Standalone Cluster
Spark Yarn Cluster