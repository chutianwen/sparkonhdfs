#!/bin/bash
export JAVA_HOME=/usr/java/jdk1.8.0_101
export PATH=$JAVA_HOME/bin:$PATH

INFILE=$1
OUTFILE=$2

echo "Original Log: "$INFILE
echo "Target Log:"$OUTFILE
/opt/spark-2.0/bin/spark-submit \
         --name "PaserLog" \
         --class "SparkHDFSLogParser" \
         --master local[96] \
         target/scala-2.11/logparser_2.11-1.0.jar \
         $INFILE $OUTFILE