import sbt.Keys._
import sbt._

// https://mvnrepository.com/artifact/org.apache.spark/spark-core_2.10
val lib_SparkCore = "org.apache.spark" % "spark-core_2.10" % "2.0.0"

// https://mvnrepository.com/artifact/org.apache.spark/spark-sql_2.10
val lib_SparkSQL = "org.apache.spark" % "spark-sql_2.10" % "2.0.0"


// define the common properties for all the projects
lazy val commonSettings = Seq(
    version := "1.0",
    scalaVersion := "2.11.8"
)

lazy val logTest = (project in file(".")).
    settings(commonSettings: _*).
    settings(
        name := "LogTest",

        // libraryDependencies: SettingKey[Seq[ModuleID]]
        libraryDependencies ++= Seq(lib_SparkCore, lib_SparkSQL)
    ).
    settings(
        mainClass in (Runtime, run) := Some("logTest")
    )
