#!/bin/bash
export JAVA_HOME=/usr/java/jdk1.8.0_101
export PATH=$JAVA_HOME/bin:$PATH
#export _JAVA_OPTIONS="-Xms10g -Xmx10g -Xss1M -XX:+UseParallelGC -XX:ParallelGCThreads=1"
# -XX:GCTimeLimit=50 -XX:GCHeapFreeLimit=10 -XX:+UseNUMA"

#export _JAVA_OPTIONS="-Xms80g -Xmx80g -XX:+UseG1GC -XX:InitiatingHeapOccupancyPercent=35"
#echo "Package to jar ..."

#/opt/sbt/bin/sbt package

echo "Running the file by spark-submit ..."

_now="$(sed -e 's/\//_/g;s/:/_/g' <<<$(date +'%D_%T'))"

#--class "org.fedcentric.spark.thousandGenome.RunGraph"
#--class "org.fedcentric.spark.thousandGenome.RunGraph"
num_iterations=3
for idx in `seq 2 $num_iterations`;
do
   #echo $(($idx*12))
   #echo $_now.$idx
   /opt/spark-2.0/bin/spark-submit \
      --class "ReadTest" \
      --master spark://10.1.10.40:7077 \
      --executor-memory 25G \
      --total-executor-cores $(($idx*12)) \
      --conf spark.executor.cores=1 \
      --conf spark.driver.cores=1 \
      --conf spark.driver.memory=5g \
      --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
      --conf spark.kryoserializer.buffer.max=256m \
      --packages logging.log4j:log4j-core:2.7 \
      --packages logging.log4j:log4j-api:2.6.2 \
      ../HDFSBenchMark/target/scala-2.11/hdfsbenchmark_2.11-1.0.jar
      #--packages graphframes:graphframes:0.2.0-spark2.0-s_2.11 \
      #target/scala-2.11/sparkprojects_2.11-1.0.jar \
      #$_now.$idx
done


num_iterations=3
for idx in `seq 2 $num_iterations`;
do
   echo $(($idx*12))
   echo $_now.$idx
   /opt/spark-2.0/bin/spark-submit \
      --class "org.fedcentric.spark.thousandGenome.RunGraph" \
      --master spark://10.1.10.40:7077 \
      --executor-memory 25G \
      --total-executor-cores $(($idx*12)) \
      --conf spark.executor.cores=1 \
      --conf spark.driver.cores=1 \
      --conf spark.driver.memory=5g \
      --conf spark.serializer=org.apache.spark.serializer.KryoSerializer \
      --conf spark.kryoserializer.buffer.max=256m \
      packages graphframes:graphframes:0.2.0-spark2.0-s_2.11 \
      target/scala-2.11/sparkprojects_2.11-1.0.jar \
      $_now.$idx
done