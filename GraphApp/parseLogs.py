out = open("results.csv", "w")

out.write("Driver Cores,Driver Memory,Total Executor Cores,Cores per Executor,Executor Memory, Test 1,Test 2,Test 3,Source\n")

first = True

with open("./test.log") as f:
  read_data = f.readlines()
  for line in read_data:
    if "driver-cores" in line:
      if not first:
        out.write("\n")
      first = False
      properties = line.split(',')
      for prop in properties:
        out.write(prop.split("=")[1] + ",")
    if "DEBUG" in line:
      right = line.split("(")[1]
      seconds = right.split(")")[0]
      out.write(seconds + ",")
f.close()
out.close()
