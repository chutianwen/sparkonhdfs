#!/bin/bash
export JAVA_HOME=/usr/java/jdk1.8.0_101
export PATH=$JAVA_HOME/bin:$PATH
export SPARK_CONF_DIR=`pwd`/SparkConf

infile="hdfs://10.1.10.40/VCFs/small3.txt"
outfile="hdfs://10.1.10.40/GraphStorage"

while true;
do
  case "$1" in
    --infile) INFILE="$2"; shift 2 ;;
    --outfile) OUTFILE="$2"; shift 2 ;;
    --) shift; break ;;
    *) break ;;
  esac
done

for driver_cores in `seq 1 4`;
do
for total_executor_cores in `seq 12 12 $((192-$driver_cores))`;
do
for executor_cores in `seq 1 4`
do
for executor_memory in `seq 20 10 100`
do
  ./runSingleStandalone.bash \
    --driver-cores $driver_cores \
    --total-executor-cores $total_executor_cores \
    --executor-cores $executor_cores \
    --executor-memory $(($executor_memory))G \
    --num-iterations 3 \
    --infile $infile --outfile $outfile
done
done
done
done
