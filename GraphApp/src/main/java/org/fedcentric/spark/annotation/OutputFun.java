package org.fedcentric.spark.annotation;

/**
 * Created by Tianwen on 8/9/2016.
 */
import java.lang.annotation.*;

/**
 * Annotates fields in objects that should be used
 * Any field annotated with @Input can
 */
@Documented
@Inherited
@Retention(RetentionPolicy.CLASS)
@Target(ElementType.METHOD)
public @interface OutputFun {

    /**
     *
     * @return
     */
    String value() default "";

    /**
     *
     * @return
     */
    String valueType() default "";
}
