/**
 * Created by Tianwen on 7/29/2016.
 */
package org.fedcentric.spark.graph

/**
 *
 */
trait Ingester{
    val input:Any
}

trait StoreIntoParquet extends Ingester{

    def save(output:String)
}

trait StoreIntoMemSQL extends Ingester {
    var Connection_Para = scala.collection.mutable.HashMap[String, Object](
        "dbHost" -> "10.1.10.40",
        "dbPort" -> "",
        "driver_memory" -> "",
        "executor_memory" -> "",
        "MasterURL" -> ""
    )
}

trait StoreIntoCSV extends Ingester {

}