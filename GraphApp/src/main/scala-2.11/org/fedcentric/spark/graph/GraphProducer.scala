/**
 * Created by Tianwen on 8/17/2016.
 */
package org.fedcentric.spark.graph

import org.fedcentric.spark.annotation.OutputFun

/**
 * Given a parsed data as input, produce the edges and nodes for next step
 */
trait GraphProducer extends Serializable{

//    @Input(value = "ParsedData", valueType = "ParsedDataType")
    val input:Any

    @OutputFun(value = "NodeAndEdge", valueType = "Dataframe or RDD[NODE]")
    protected def CreateNodeEdgesGraphX():(Any, Any)

    protected def CreateNodeEdgesGraphFrame():(Any, Any)
}