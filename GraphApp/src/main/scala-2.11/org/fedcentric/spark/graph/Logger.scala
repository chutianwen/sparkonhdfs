package org.fedcentric.spark.graph

import org.apache.log4j.LogManager

trait Logger extends Serializable{
    /**
     * Read from $SPARK_HOME/conf/log4j.properties
     */
    @transient lazy val log = LogManager.getLogger("myLogger")

    @transient lazy val rootlog = LogManager.getRootLogger

    val NanoToSecond = scala.math.pow(10, -9)

    def time[R](block: => R): Double = {
        val t0 = System.nanoTime()
        block    // call-by-name
        val t1 = System.nanoTime()
        (t1 - t0)*NanoToSecond
    }
}
