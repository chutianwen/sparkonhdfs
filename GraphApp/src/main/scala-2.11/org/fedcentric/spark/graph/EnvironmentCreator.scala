/**
 * Created by Tianwen on 8/1/2016.
 */
package org.fedcentric.spark.graph

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

/**
 * Trait for different application to set up spark session
 */
trait EnvironmentCreator extends Serializable{

    /**
     * Create a Map containing parameters. <Key = ConfigPara, value>
     */
    var Map_Configs = scala.collection.mutable.HashMap[String, String](
        "num_threads_local" -> "",
        "num_executor_cores" -> "",
        "driver_memory" -> "",
        "executor_memory" -> "",
        "MasterURL" -> ""
    )

    /**
     * Create a SparkSession based on the mode chosen when executing main()
     * @param MODE_RUN 0:local, 1:standalone cluster, Default: local
     * @return sparksession
     */
    protected def CreateSparkSess(MODE_RUN:Int):SparkSession = {
        MODE_RUN match{
            case 0 => {
                println("Choosing Local Mode")
                CreateSparkSessLocal()
            }
            case 1 =>{
                println("Choosing Standalone Cluster Mode")
                CreateSparkSessClusterStandAlone()
            }
            case _ => {
                println("Using default local mode!")
                CreateSparkSessLocal()
            }
        }
    }

    /**
     *
     * @return SparkSession
     */
    protected final def CreateSparkSessLocal():SparkSession = {
        Map_Configs("num_threads_local") = "2"
        Map_Configs("driver_memory") = "2g"

        val conf_local = new SparkConf()
            .setMaster("local[%s]".format(Map_Configs("num_threads_local")))
            .setAppName("Local Test of Graph")
            .set("spark.driver.memory", Map_Configs("driver_memory"))
            .set("spark.driver.maxResultSize","0")
            .set("spark.local.dir", "/tmp/spark")
            .set("spark.memory.fraction", "0.5")
            .set("spark.network.timeout", "30m")

        val spark = SparkSession.builder().config(conf_local).getOrCreate()
        return spark
    }

    /**
     *
     * @return SparkSession
     */
    protected final def CreateSparkSessClusterStandAlone():SparkSession = {

        Map_Configs("MasterURL") = "spark://10.1.10.40:7077"
        Map_Configs("driver_memory") = "100g"
        Map_Configs("num_executor_cores") = "12"

        val conf_cluster = new SparkConf()
            .setMaster(Map_Configs("MasterURL"))
            .setAppName("Cluster Test of Graph")
            .set("spark.driver.maxResultSize","0")
            .set("spark.driver.memory", Map_Configs("driver_memory"))
            .set("spark.executor.cores", Map_Configs("num_executor_cores"))


        val spark = SparkSession.builder().config(conf_cluster).getOrCreate()
        return spark
    }
}