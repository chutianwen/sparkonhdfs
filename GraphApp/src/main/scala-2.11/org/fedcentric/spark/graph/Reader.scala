/**
 * Created by Tianwen on 8/2/2016.
 */

package org.fedcentric.spark.graph

import org.apache.hadoop.fs.Path
import org.fedcentric.spark.annotation.OutputFun

/**
 * The Parent trait of parser for all types of data, like
 * CancerVCF, Virus, Pediatric Caner
 * DataType: VCF, textfile, csv, xml...
 */
trait Reader extends Serializable{

    /**
     * Path to source data, mostly String type
     * Can be path on Local OS system or HDFS on cluster
     */
    val input:Any

    /**
     * Loading different data types into Spark RDD or Dataframe
     * @param RawDataPath
     * @param Mode_Read only functions if RawDataPath is a dir
     *                  0: Doing file in dir one by one
     *                  1: Doing all files together
     * @return
     */
    protected def LoadData(Mode_Read:Int): List[Any]

    /**
     * Loading from single file
     * @param FileName
     * @return
     */
    protected def LoadDataFromFile(FileName:String): List[Any]

    /**
     * (Local Os Disk Case)
     * When input indicates a directory rather than a file, then providing two ways to process
     * 0. Processing files in directory one by one
     * 1. Joining all the files in directory by ",", let spark read it as a whole.
     * @param DirName
     * @param Mode_read
     * @return
     */
    protected def LoadDataFromDir(DirName:String, Mode_read:Int): List[Any]

    /**
     * (HDFS Case)
     * When input indicates a directory rather than a file, then providing two ways to process
     * 0. Processing files in directory one by one
     * 1. Joining all the files in directory by ",", let spark read it as a whole.
     * @param DirName
     * @param Mode_read
     * @return
     */
    protected def LoadDataFromDirHDFS(DirName: Path, Mode_read: Int): List[String]

}