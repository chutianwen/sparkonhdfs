/**
 * Created by Tianwen on 7/29/2016.
 */
package org.fedcentric.spark.graph

import org.fedcentric.spark.annotation.OutputFun

/**
 * The Parent trait of parser for all types of data, like
 * CancerVCF, Virus, Pediatric Caner
 * DataType: VCF, textfile, csv, xml...
 */
trait Parser extends Serializable{

//    @Input(value = "LoadedData", valueType = "Self-defined Structure")
    val input:Any

    /**
     * Function to call in main()
     * @return The parsed result
     */
    @OutputFun(value = "ParsedData", valueType = "ParsedRDD or ParsedDataFrame")
    protected def Parse():Any
}