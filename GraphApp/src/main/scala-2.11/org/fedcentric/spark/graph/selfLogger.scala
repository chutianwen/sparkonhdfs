package org.fedcentric.spark.graph

import java.io.{File, FileOutputStream, PrintWriter}

trait SelfLogger{
    def log(msg:String):Unit
}

trait FileLogger extends SelfLogger{

    var LogfileName:String = "/home/tchu/Desktop/log.txt"
   // lazy val fileOutput=new PrintWriter(LogfileName:String)
    val fileOutput= new PrintWriter(new FileOutputStream(new File(LogfileName),true))

    def log(msg:String):Unit={
        fileOutput.print(msg)
        fileOutput.flush()
    }
}
