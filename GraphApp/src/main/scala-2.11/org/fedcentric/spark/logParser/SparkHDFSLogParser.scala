package org.fedcentric.spark.logParser

import java.io.FileWriter
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object SparkHDFSLogParser {
    def main(args: Array[String]): Unit = {

        val logPath = args(0)
        val savePath = args(1)

        val conf_cluster = new SparkConf()
        val spark = SparkSession.builder().config(conf_cluster).getOrCreate()
        val logRDD = spark.sparkContext.textFile(logPath).filter(!_.isEmpty).cache

        val MetaRDD = logRDD.filter(!_.startsWith("["))
        val TimeRDD = logRDD.filter(_.startsWith("["))

        // Get the headers
        var header = MetaRDD.first().split(",").map(_.split("=")(0))
        val numIteration = 3
        val headerTests = (for(i <- 1 to numIteration) yield "Test.%d".format(i)).toArray
        header ++= headerTests
        val headerString = header.mkString("\t")
        // Split the fields in MetaRDD
        val FieldsRDD = MetaRDD.map( line => line.split(",").map(_.split("=")(1)).mkString("\t") )

        val TestTimesRDD = TimeRDD.map( line => line.split("\\(")(1).split("\\)")(0) )

        val FieldsCollect = FieldsRDD.collect()
        val TimeCollect = TestTimesRDD.collect().grouped(numIteration).toArray.map(_.mkString("\t"))

        val ZipRes = FieldsCollect zip TimeCollect
        val ZipResCombine = ZipRes.map(x => "%s\t%s".format(x._1, x._2))

        val fileWriter = new FileWriter(savePath)

        fileWriter.write(headerString + "\n")
        ZipResCombine.foreach(line => fileWriter.write(line + "\n"))
        fileWriter.flush()
        fileWriter.close()

        spark.stop()
    }
}