/**
* Created by Tianwen on 8/17/2016.
*/

package org.fedcentric.spark.thousandGenome

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.fedcentric.spark.annotation.Input
import org.fedcentric.spark.graph.GraphProducer
import org.json4s.DefaultFormats
import org.json4s.jackson.JsonMethods._

import scala.collection.{mutable, Map}
import scala.collection.mutable.ArrayBuffer
import scala.io.Source


trait GeneratorVertexID{

    // start id for different types of node
    protected final val StartID_Superpop:scala.Long = 100L
    protected final val StartID_Pop:scala.Long = 1000L
    protected final val StartID_Ind:scala.Long = 10000L

    // bucket sizes
    protected final val bucketsize_Chrom:scala.Long = 1000000000000L
    protected final val bucketsize_Location:scala.Long = 1000L
    protected final val bucketsize_Allele:scala.Long = 100L

    /**
     * Generating the vertex ID for different nodes depending on the node type
     * According to the starting location, these are self-defined hashcode function.
     * @param nodetype
     * @param id_superpop
     * @param id_pop
     * @param id_Ind
     * @param start_loc
     * @param id_ref
     * @param id_alt
     * @return VertexID
     */
    protected def GetVertexID(nodetype:String,
                            id_superpop:scala.Long = 0,
                            id_pop:scala.Long = 0,
                            id_ind:scala.Long = 0,
                            id_chrom:scala.Long = 0,
                            start_loc:scala.Long = 0,
                            id_ref:scala.Long = 0,
                            id_alt:scala.Long = 0):scala.Long = {
        nodetype match{
            case "SuperPop" => {
                StartID_Superpop + id_superpop
            }
            case "Pop" => {
                StartID_Pop + id_pop
            }
            case "Ind" => {
                StartID_Ind + id_ind
            }
            case "Chrom" => {
                id_chrom*bucketsize_Chrom
            }
            case "RefAllele"=>{
                id_chrom*bucketsize_Chrom + start_loc*bucketsize_Location + id_ref
            }
            case "AltAllele"=>{
                id_chrom*bucketsize_Chrom + start_loc*bucketsize_Location + id_ref*bucketsize_Allele + id_alt
            }
            case _ => -1
        }
    }
}

class GraphProducerVCF(@Input(value = "ParsedData", valueType = "ParsedDataType")
                       override val input:RDD[ParsedLine])
    extends GraphProducer with GeneratorVertexID{

    /**
     * GraphFrame Version:
     * @return
     */
    private def CreateNodeEdgeFromParsedDataGraphFrame():(RDD[NodeDF], RDD[EdgeDF]) = {

        // !!!cannot directly using GraphProducerVCF.broadcast_Dict_Ind_VertexId as para in next function
        // !!! Have to figure out why this is necessary
        val broadcast_Dict_Ind_VertexId = GraphProducerVCF.broadcast_Dict_Ind_VertexId
        val res = input.map(line=> HelperCreateNodeEdgeFromParsedDataGraphFrame(line, broadcast_Dict_Ind_VertexId))
        @transient val nodes = res.flatMap(x => x._1)
        @transient val edges = res.flatMap(x => x._2)
        (nodes, edges)
    }

    private def HelperCreateNodeEdgeFromParsedDataGraphFrame(parsedLine: ParsedLine, broadcast_Dict_Ind_VertexId:BroadCast_Sample_ID):
    (ArrayBuffer[NodeDF], ArrayBuffer[EdgeDF]) = {

        val Node_Buffer = new ArrayBuffer[NodeDF]
        val Edge_Buffer = new ArrayBuffer[EdgeDF]
        val id_Chrom:scala.Long = parsedLine._1("Chrom") match {
            case "X" => 23
            case "Y" => 24
            case _ =>{
                // May add try catch here
                parsedLine._1("Chrom").toLong
            }
        }
        val Start_loc = parsedLine._1("Ref_Start_pos").toLong
        val Stop_loc = parsedLine._1("Ref_Stop_pos").toLong
        val id_Ref:scala.Long = 0
        val vertexID_ref = GetVertexID("RefAllele", id_chrom = id_Chrom, start_loc = Start_loc, id_ref = id_Ref)
        val ref_allele = parsedLine._1("Ref_allele")
        // Adding Ref Allele Node
        Node_Buffer.append(NodeDF(vertexID_ref, "Ref_allele", ref_allele, Start_loc, Stop_loc))
        val vertexID_Chrom:scala.Long = GetVertexID("Chrom", id_chrom = id_Chrom)

        // Adding Edge from chrom to Ref Allele
        Edge_Buffer.append(EdgeDF(vertexID_Chrom, vertexID_ref, "Chrom_to_ref"))

        var id_Alt:scala.Long = 1
        for(variants <- parsedLine._2){
            val alt_allele = variants._1("Alt_alleles")
            val vertexID_AltAllele = GetVertexID("AltAllele", id_chrom = id_Chrom, start_loc = Start_loc, id_ref = id_Ref, id_alt = id_Alt)
            // Adding Alt Allele Node
            Node_Buffer.append(NodeDF(vertexID_AltAllele, "Alt_allele", alt_allele, infoFeature = variants._1.toString)) //

            // Adding Edge from Ref to Alt
            Edge_Buffer.append(EdgeDF(vertexID_ref, vertexID_AltAllele, "Ref_to_alt", variants._1("VT")))

            for(sample <- variants._2){
                // Adding Edge from Ind to Alt, "He"
                Edge_Buffer.append(EdgeDF(broadcast_Dict_Ind_VertexId.value(sample), vertexID_AltAllele, "Ind_to_alt", "He"))
            }
            for(sample <- variants._3){
                // Adding Edge from Ind to Alt, "Ho"
                Edge_Buffer.append(EdgeDF(broadcast_Dict_Ind_VertexId.value(sample), vertexID_AltAllele, "Ind_to_alt", "Ho"))
            }
            id_Alt += 1
        }
        (Node_Buffer, Edge_Buffer)
    }

    /**
     * GraphX Version:
     * @return
     */
    @transient
    private def CreateNodeEdgeFromParsedDataGraphX():(RDD[NODE], RDD[EDGE]) = {
        val res = input.map(line=> HelperCreateNodeEdgeFromParsedDataGraphX(line, GraphProducerVCF.broadcast_Dict_Ind_VertexId))
        val nodes = res.flatMap(x => x._1)
        val edges = res.flatMap(x => x._2)
        (nodes, edges)
    }

    @transient
    private def HelperCreateNodeEdgeFromParsedDataGraphX(parsedLine: ParsedLine, broadcast_Dict_Ind_VertexId:BroadCast_Sample_ID):
    (ArrayBuffer[NODE], ArrayBuffer[EDGE]) = {

        val Node_Buffer = new ArrayBuffer[NODE]
        val Edge_Buffer = new ArrayBuffer[EDGE]
        val id_Chrom:scala.Long = parsedLine._1("Chrom") match {
            case "X" => 23
            case "Y" => 24
            case _ =>{
                // May add try catch here
                parsedLine._1("Chrom").toLong
            }
        }
        val Start_loc = parsedLine._1("Ref_Start_pos").toLong
        val Stop_loc = parsedLine._1("Ref_Stop_pos").toLong
        val id_Ref:scala.Long = 0
        val vertexID_ref = GetVertexID("RefAllele", id_chrom = id_Chrom, start_loc = Start_loc, id_ref = id_Ref)
        val ref_allele = parsedLine._1("Ref_allele")
        // Adding Ref Allele Node
        Node_Buffer.append((vertexID_ref, Node_Ref("Ref_allele", ref_allele, Start_loc, Stop_loc)))
        val vertexID_Chrom:scala.Long = GetVertexID("Chrom", id_chrom = id_Chrom)

        // Adding Edge from chrom to Ref Allele
        Edge_Buffer.append((vertexID_Chrom, vertexID_ref, Edge_Chrom_to_Ref("Chrom_to_ref")))

        var id_Alt:scala.Long = 1
        for(variants <- parsedLine._2){
            val alt_allele = variants._1("Alt_alleles")
            val vertexID_AltAllele = GetVertexID("AltAllele", id_chrom = id_Chrom, start_loc = Start_loc, id_ref = id_Ref, id_alt = id_Alt)
            // Adding Alt Allele Node
            Node_Buffer.append((vertexID_AltAllele, Node_Alt("Alt_allele", alt_allele, variants._1))) //
//            println((vertexID_AltAllele, Node_Alt("Alt_allele", alt_allele, variants._1)))
            // Adding Edge from Ref to Alt
            Edge_Buffer.append((vertexID_ref, vertexID_AltAllele, Edge_Ref_to_Alt("Ref_to_alt", variants._1("VT"))))
            for(sample <- variants._2){
                // Adding Edge from Ind to Alt, "He"
                Edge_Buffer.append((broadcast_Dict_Ind_VertexId.value(sample), vertexID_AltAllele, Edge_Ind_to_Alt("Ind_to_alt", "He")))
            }
            for(sample <- variants._3){
                // Adding Edge from Ind to Alt, "Ho"
                Edge_Buffer.append((broadcast_Dict_Ind_VertexId.value(sample), vertexID_AltAllele, Edge_Ind_to_Alt("Ind_to_alt", "Ho")))
            }
            id_Alt += 1
        }
        (Node_Buffer, Edge_Buffer)
    }

    /**
     * GraphX Version
     * Output function, create all the nodes and edges for creating graph
     * @param mode 0: GraphX version
     *             1: GraphFrame version
     * @return node and edge tuple
     */
    @transient
    override def CreateNodeEdgesGraphX(): (RDD[NODE], RDD[EDGE]) = {
        val res3 = CreateNodeEdgeFromParsedDataGraphX()
        (res3._1, res3._2)
    }
    /**
     * GraphFrame Version
     * Output function, create all the nodes and edges for creating graph
     * @param mode 0: GraphX version
     *             1: GraphFrame version
     * @return node and edge tuple
     */
    @transient
    override def CreateNodeEdgesGraphFrame(): (RDD[NodeDF], RDD[EdgeDF]) = {
        val res3 = CreateNodeEdgeFromParsedDataGraphFrame()
//    	(res3._1.distinct(), res3._2.distinct())
	(res3._1, res3._2) 
   }

}

/**
* Define companion object for storing all the paths of additional data
*/
object GraphProducerVCF extends GeneratorVertexID{

    val path_json_pops = "/home/schneiderj/sparkonhdfs/GraphApp/src/main/resources/%s".format("thousandGenome/Populations.txt")
    val path_json_ind = "/home/schneiderj/sparkonhdfs/GraphApp/src/main/resources/%s".format("thousandGenome/Samples.txt")

    var broadcast_Dict_Ind_VertexId: BroadCast_Sample_ID = _
    var Dict_Pops_VertexId: PopId = Map[String, scala.Long]()

    def CreateNodeEdgesFromJsonGraphX(sparksess: SparkSession): (RDD[NODE], RDD[EDGE]) = {
        val res1 = CreateNodeEdgeFromJsonFileGraphX(sparksess)
        val res2 = CreateNodeFromPriorKnowledgeGraphX(sparksess)
        (res1._1 ++ res2, res1._2)
    }

    def CreateNodeEdgesFromJsonGraphFrame(sparksess: SparkSession): (RDD[NodeDF], RDD[EdgeDF]) = {
        val res1 = CreateNodeEdgeFromJsonFileGraphFrame(sparksess)
        val res2 = CreateNodeFromPriorKnowledgeGraphFrame(sparksess)
        (res1._1 ++ res2, res1._2 )
    }

    /**
     * GraphFrame Version:
     * Create Nodes and Edges which are parsed from the Json files on disk
     * Containing Superpop, pop and Individuals Data
     * @return
     */
    private def CreateNodeEdgeFromJsonFileGraphFrame(sparksess: SparkSession):(RDD[NodeDF], RDD[EdgeDF]) = {

        val Node_Buffer = new ArrayBuffer[NodeDF]
        val Edge_Buffer = new ArrayBuffer[EdgeDF]

        //Optimization of guessing initiated size
        Node_Buffer.sizeHint(3000)
        Edge_Buffer.sizeHint(3000)

        /*Processing Json file containing Pops and SuperPop*/
        val PopSuperpop = Source.fromFile(path_json_pops).mkString
        val JSON_PopSuperpop = parse(PopSuperpop)
        implicit val formats = DefaultFormats
        val PopToSuperpop: Map[String, String] = JSON_PopSuperpop.extract[Map[String, String]]

        var Dict_Pops_VertexId_tmp = mutable.Map[String, scala.Long]()
        var id_Pop:scala.Long = 1
        var iD_Superpop:scala.Long = 1

        var vertexID_pop:scala.Long = 0
        var vertexID_superpop:scala.Long = 0
        for(pop <- PopToSuperpop.keys){
            vertexID_pop = GetVertexID("Pop", id_pop = id_Pop)
            Dict_Pops_VertexId_tmp += (pop -> vertexID_pop)
            Node_Buffer.append(NodeDF(vertexID_pop, nodetype = "Pop", name = pop))
            if(!Dict_Pops_VertexId_tmp.contains(PopToSuperpop(pop))){
                vertexID_superpop = GetVertexID("SuperPop", id_superpop = iD_Superpop)
                Dict_Pops_VertexId_tmp += (PopToSuperpop(pop) -> vertexID_superpop)
                Node_Buffer.append(NodeDF(vertexID_superpop, nodetype = "SuperPop", name = pop))
                iD_Superpop += 1
            }
            id_Pop += 1
            Edge_Buffer.append(EdgeDF(vertexID_superpop, vertexID_pop, "SuperPop_to_pop"))
        }
        Dict_Pops_VertexId = Dict_Pops_VertexId_tmp.toMap

        /*Processing Json file containing Ind and Pop info*/
        val IndPop = Source.fromFile(path_json_ind).mkString
        val JSON_IndPop = parse(IndPop)
        val IndToPop: Map[String, String] = JSON_IndPop.extract[Map[String, String]]
        var Dict_Inds_VertexId_tmp = mutable.Map[String, scala.Long]()
        var id_Ind:scala.Long = 1
        var vertexID_ind:scala.Long = 0
        for(ind_pop <- IndToPop){
            vertexID_ind = GetVertexID("Ind", id_ind = id_Ind)
            Dict_Inds_VertexId_tmp += (ind_pop._1 -> vertexID_ind)
            Node_Buffer.append(NodeDF(vertexID_ind, nodetype = "Ind", name =ind_pop._1))
            Edge_Buffer.append(EdgeDF(Dict_Pops_VertexId(ind_pop._2), vertexID_ind, relationship = "Pop_to_ind"))
            id_Ind += 1
        }
        broadcast_Dict_Ind_VertexId = sparksess.sparkContext.broadcast(Dict_Inds_VertexId_tmp.toMap)
        // return the Node and Edge RDD
        (sparksess.sparkContext.parallelize(Node_Buffer),
            sparksess.sparkContext.parallelize(Edge_Buffer))
    }

    /**
     * GraphX Version:
     * Create Nodes and Edges which are parsed from the Json files on disk
     * Containing Superpop, pop and Individuals Data
     * @return
     */
    private def CreateNodeEdgeFromJsonFileGraphX(sparksess: SparkSession):(RDD[NODE], RDD[EDGE]) = {

        val Node_Buffer = new ArrayBuffer[NODE]
        val Edge_Buffer = new ArrayBuffer[EDGE]

        //Optimization of guessing initiated size
        Node_Buffer.sizeHint(3000)
        Edge_Buffer.sizeHint(3000)

        /*Processing Json file containing Pops and SuperPop*/
        val PopSuperpop = Source.fromFile(GraphProducerVCF.path_json_pops).mkString
        val JSON_PopSuperpop = parse(PopSuperpop)
        implicit val formats = DefaultFormats
        val PopToSuperpop: Map[String, String] = JSON_PopSuperpop.extract[Map[String, String]]

        var Dict_Pops_VertexId_tmp = mutable.Map[String, scala.Long]()
        var id_Pop:scala.Long = 1
        var iD_Superpop:scala.Long = 1

        var vertexID_pop:scala.Long = 0
        var vertexID_superpop:scala.Long = 0
        for(pop <- PopToSuperpop.keys){
            vertexID_pop = GetVertexID("Pop", id_pop = id_Pop)
            Dict_Pops_VertexId_tmp += (pop -> vertexID_pop)
            Node_Buffer.append((vertexID_pop, Node_Pop(nodetype = "Pop", name = pop)))
            if(!Dict_Pops_VertexId_tmp.contains(PopToSuperpop(pop))){
                vertexID_superpop = GetVertexID("SuperPop", id_superpop = iD_Superpop)
                Dict_Pops_VertexId_tmp += (PopToSuperpop(pop) -> vertexID_superpop)
                Node_Buffer.append((vertexID_superpop, Node_SuperPop(nodetype = "Pop", name = pop)))
                iD_Superpop += 1
            }
            id_Pop += 1
            Edge_Buffer.append((vertexID_superpop, vertexID_pop, Edge_SuperPop_to_Pop("SuperPop_to_pop")))
        }
        Dict_Pops_VertexId = Dict_Pops_VertexId_tmp.toMap

        /*Processing Json file containing Ind and Pop info*/
        val IndPop = Source.fromFile(GraphProducerVCF.path_json_ind).mkString
        val JSON_IndPop = parse(IndPop)
        val IndToPop: Map[String, String] = JSON_IndPop.extract[Map[String, String]]
        var Dict_Inds_VertexId_tmp = mutable.Map[String, scala.Long]()
        var id_Ind:scala.Long = 1
        var vertexID_ind:scala.Long = 0
        for(ind_pop <- IndToPop){
            vertexID_ind = GetVertexID("Ind", id_ind = id_Ind)
            Dict_Inds_VertexId_tmp += (ind_pop._1 -> vertexID_ind)
            Node_Buffer.append((vertexID_ind, Node_Ind(nodetype = "Ind", name =ind_pop._1)))
            Edge_Buffer.append((Dict_Pops_VertexId(ind_pop._2), vertexID_ind, Edge_Pop_to_Ind(relationship = "Pop_to_ind")))
            id_Ind += 1
        }
        broadcast_Dict_Ind_VertexId = sparksess.sparkContext.broadcast(Dict_Inds_VertexId_tmp.toMap)
        // return the Node and Edge RDD
        (sparksess.sparkContext.parallelize(Node_Buffer),
         sparksess.sparkContext.parallelize(Edge_Buffer))
    }

    /**
     * GraphFrame Version:
     * Creating all 24 chromosome nodes
     * @return
     */
    private def CreateNodeFromPriorKnowledgeGraphFrame(sparksess: SparkSession):RDD[NodeDF] = {
        val Node_Buffer = new ArrayBuffer[NodeDF]
        Node_Buffer.sizeHint(24)
        var vertexID_Chrom:scala.Long = 0
        for(id_Chrom <- 1 to 24){
            vertexID_Chrom = GetVertexID("Chrom", id_chrom = id_Chrom)
            id_Chrom match{
                case 23 => Node_Buffer.append(NodeDF(vertexID_Chrom, nodetype = "Chrom", name = "X"))
                case 24 => Node_Buffer.append(NodeDF(vertexID_Chrom, nodetype = "Chrom", name = "Y"))
                case _ => Node_Buffer.append(NodeDF(vertexID_Chrom, nodetype = "Chrom", name = id_Chrom.toString))
            }
        }
        // return Chrom Nodes
        sparksess.sparkContext.parallelize(Node_Buffer)
    }


    /**
     * GraphX Version:
     * Creating all 24 chromosome nodes
     * @return
     */
    private def CreateNodeFromPriorKnowledgeGraphX(sparksess: SparkSession):RDD[NODE] = {
        val Node_Buffer = new ArrayBuffer[NODE]
        Node_Buffer.sizeHint(24)
        var vertexID_Chrom:scala.Long = 0
        for(id_Chrom <- 1 to 24){
            vertexID_Chrom = GetVertexID("Chrom", id_chrom = id_Chrom)
            //            println(vertexID_Chrom + " " + id_Chrom)
            id_Chrom match{
                case 23 => Node_Buffer.append((vertexID_Chrom, Node_Chrom(nodetype = "Chrom", name = "X")))
                case 24 => Node_Buffer.append((vertexID_Chrom, Node_Chrom(nodetype = "Chrom", name = "Y")))
                case _ => Node_Buffer.append((vertexID_Chrom, Node_Chrom(nodetype = "Chrom", name = id_Chrom.toString)))
            }
        }
        // return Chrom Nodes
        sparksess.sparkContext.parallelize(Node_Buffer)
    }

}
