
package org.fedcentric.spark.thousandGenome
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.SaveMode.Overwrite
import org.fedcentric.spark.graph.StoreIntoParquet

class StoreIntoParquetVCF(override val input:DataFrame) extends StoreIntoParquet {

    def save(OutputURL: String): Unit = {
        input.write.format("parquet").mode(Overwrite).save(OutputURL)
    }
}




