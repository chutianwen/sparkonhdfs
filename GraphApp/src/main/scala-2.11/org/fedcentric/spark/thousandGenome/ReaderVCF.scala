package org.fedcentric.spark.thousandGenome
import org.apache.spark.sql.SparkSession
import org.fedcentric.spark.graph.Reader
import org.fedcentric.spark.annotation._
import org.apache.hadoop.fs.Path
import java.io.{FileNotFoundException, File}
import scala.collection.mutable.ListBuffer
import org.fedcentric.spark.graph.Logger
/**
 * Loading the VCF into RDD, then parse metadata and header
 * @param sc
 */
class ReaderVCF(@Input(value = "PathToRawData", valueType = "String") val input:String,
                val sparksess: SparkSession) extends Reader with Logger{

    private val conf = sparksess.sparkContext.hadoopConfiguration
    private val fs = org.apache.hadoop.fs.FileSystem.get(conf)

    /**
     * Load the vcf data from disk
     * @param Mode_read 0:
     * @return self-defined VCF class, containing the
     */
    @throws(classOf[FileNotFoundException])
    @OutputFun(value = "LoadedData", valueType = "List of String")
    override def LoadData(Mode_read: Int = 0): List[String] = {

        // Determine if it is a HDFS path or regular OS path
        if (input.matches(".*(?i)hdfs.*")) {
            log.info("--reading from HDFS")
            // Also should look for if it have multiple files defined by karma ','
            val inputPaths = input.split(",")
            val validPaths = inputPaths.map(new Path(_)).filter(fs.exists)
            val validFilePaths = validPaths.filter(fs.isFile).map(_.toString).toList
            val validDirPaths = validPaths.filter(fs.isDirectory)

            val files = new ListBuffer[String]()
            files.appendAll(validFilePaths)
            for(validDirPath <- validDirPaths){
                if(Mode_read == 0){
                    files.appendAll( LoadDataFromDirHDFS(validDirPath, 0) )
                }else{
                    // each return is a string containing all the files in one dir joined by ","
                    files.appendAll( LoadDataFromDirHDFS(validDirPath) )
                }
            }
            log.info("--Number of files:%d".format(files.length))
            files.toList
        } else {
            // Also should look for if it have multiple files defined by karma ','
            val inputPaths = input.split(",")
            // check if the input contains multiple file including ","
            val validPaths = inputPaths.map(new File(_)).filter(_.exists())
            val validFilePaths = validPaths.filter(_.isFile).map(_.getPath).toList
            val validDirPaths = validPaths.filter(_.isDirectory).map(_.getPath)

            val files = new ListBuffer[String]()
            files.appendAll(validFilePaths)
            for(validDirPath <- validDirPaths){
                if(Mode_read == 0){
                    files.appendAll( LoadDataFromDir(validDirPath, 0) )
                }else{
                    // each return is a string containing all the files in one dir joined by ","
                    files.appendAll( LoadDataFromDir(validDirPath) )
                }
            }
            files.toList
        }
    }

    /**
     *
     * @param FileName
     * @return
     */
    @inline
    protected override def LoadDataFromFile(FileName: String): List[String]= {
        List(FileName)
    }

    /**
     * Return the filePath which will loaded by spark (From OS)
     * @param DirName
     * @param Mode_read 0: Spark will read the vcf iteratively
     *                  _: Read all the vcf as a whole, using "," to join all the files
     * @return  filePath which will loaded by spark
     */
    protected override def LoadDataFromDir(DirName: String, Mode_read: Int = -1): List[String] = {
        // return the directory tree, root is current Dir
        def recursiveListFiles(f: File): Array[File] = {
            val files = f.listFiles
            files ++ files.filter(_.isDirectory).flatMap(recursiveListFiles)
        }
        val files = recursiveListFiles(new File(DirName)).map(_.getName).filter(_.startsWith("ALL.chr")).toList

        if(Mode_read == 0){
            files
        }else{
            List(files.mkString(","))
        }
    }

    /**
     * Return the filePath which will loaded by spark (From HDFS)
     * @param DirName
     * @param Mode_read 0: Spark will read the vcf iteratively
     *                  _: Read all the vcf as a whole, using "," to join all the files
     * @return  filePath which will loaded by spark
     */
    protected def LoadDataFromDirHDFS(DirName: Path, Mode_read: Int = -1): List[String] = {

        val filesInDir = fs.listFiles(DirName, true)
        var files:List[Path] = List()
        while(filesInDir.hasNext){
            files:+=filesInDir.next().getPath
        }

        val filesFiltered = files.filter(x => x.getName.startsWith("ALL.chr")).map(_.toString)
        if(Mode_read == 0){
            filesFiltered
        }else{
            List(filesFiltered.mkString(","))
        }
    }
}