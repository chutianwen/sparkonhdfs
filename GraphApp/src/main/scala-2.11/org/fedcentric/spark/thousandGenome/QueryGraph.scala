/**
 * Created by Tianwen on 8/23/2016.
 */

package org.fedcentric.spark.thousandGenome

import java.io.{File, PrintWriter}

import org.apache.spark.sql.SaveMode.Overwrite
import org.fedcentric.spark.graph.EnvironmentCreator

/**
 * The main job is here, creating graph
 */
object QueryGraph extends EnvironmentCreator {

    /*
    def consolidae(dirPath: String, targetDir:String) = {
        val dirs = new File(dirPath)
        for(dir <- dirs.listFiles()){
            val dir_path = dir.getPath
            val df = spark.sqlContext.read.load(dir_path)
            var numParts = (dir.listFiles().size/275)
            if(numParts == 0){
                numParts = 1
            }
            df.coalesce(numParts).write.format("parquet").mode(Overwrite).save("%s/%s".format(targetDir,dir_path.split('/').last))
        }
    }
    consolidae("/sandisk_m1/ThousandGenome_final/Nodes", "/sandisk_m1/ThousandGenome_levelOne_Consolidated/Nodes")
    */
//    val dir = new File("/sandisk_m1/ThousandGenome_consolidated/Edges/Edge_ALL.chr15.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf")
//    val df = spark.sqlContext.read.load(dir)
//    var numParts = (dir.listFiles().size/5).toInt
//    if(numParts == 0){
//        numParts = 1
//    }
//    df.coalesce(numParts).write.format("parquet").mode(Append).save("/sandisk_m1/ThousandGenome_Total_Consolidated/Edges/Edge_ALL.chr15.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf")



    /**
     *
     * @param args Mode between "cluster" or "local"
     */
    def main(args: Array[String]): Unit = {

        val flag = 1
        val spark = CreateSparkSess(flag)
        import org.graphframes._

        import spark.implicits._
        val ndf = spark.sqlContext.read.load("/sandisk_m1/ThousandGenome_Total_Consolidated/Nodes/*")
        val edf = spark.sqlContext.read.load("/sandisk_m1/ThousandGenome_Total_Consolidated/Edges/*")
        val g = GraphFrame(ndf, edf)
        val pw1 = new PrintWriter(new File("Report.txt" ))


        var t_st : scala.Float = System.nanoTime()
        val num_v = g.vertices.cache.count
        var t_ed : scala.Float = System.nanoTime()
        pw1.write("Number of vertices in whole graph: %d ".format(num_v))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")

        t_st = System.nanoTime()
        val num_e = g.edges.cache.count
        t_ed = System.nanoTime()
        pw1.write("Number of edges in whole graph: %d ".format(num_e))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")


        //Select Chromosome 1 Node
        t_st = System.nanoTime()
        val num_v_chrom = g.vertices.filter($"id = 1").count
        t_ed = System.nanoTime()
        pw1.write("Number of chrom1 vertices in whole graph: %d ".format(num_v_chrom))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")

        //Select references attached to Chromosome 1
        t_st = System.nanoTime()
        val num_e_from_chrom = g.edges.filter("src = 1").count
        t_ed = System.nanoTime()
        pw1.write("Number of edges in whole graph from chrom1: %d ".format(num_e_from_chrom))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")


        //Alt connected to Ref on Chromosome 1
        val subN = g.vertices.filter($"nodetype" === "Ref_allele" || $"nodetype" === "Alt_allele" ||( $"id" === 1 && $"nodetype" === "Chrom" ) )
        val subE = g.edges.filter(( $"src" === 1 && $"relationship" === "Chrom_to_ref" ) || $"relationship" === "Ref_to_alt")
        val g2 = GraphFrame(subN, subE)

        t_st = System.nanoTime()
        val num_v_g2 = g2.vertices.cache.count
        t_ed = System.nanoTime()
        pw1.write("Number of nodes in sub graph g2: %d ".format(num_v_g2))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")

        t_st = System.nanoTime()
        val num_e_g2 = g2.edges.cache.count
        t_ed = System.nanoTime()
        pw1.write("Number of edges in sub graph g2: %d ".format(num_e_g2))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")

        t_st = System.nanoTime()
        val num_alt_ref_chrom = g2.find("(a)-[ab]->(b); (b)-[bc]->(c)").filter("ab.relationship = 'Chrom_to_ref'").filter("bc.relationship = 'Ref_to_alt'").count
        t_ed = System.nanoTime()
        pw1.write("Number of alt allels in sub graph g2 connected to ref and chrom: %d ".format(num_alt_ref_chrom))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")

        t_st = System.nanoTime()
        val num_ind = g.vertices.filter("name = 'EUR'").count
        t_ed = System.nanoTime()
        pw1.write("Number of ind in EUR pop: %d ".format(num_ind))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")

        val subN3 = g.vertices.filter($"nodetype" === "Ref_allele" || $"nodetype" === "Alt_allele" ||( $"name" === "EUR" && $"nodetype" === "Population" ) || $"nodetype" === "Individual")
        val subE3 = g.edges.filter($"relationship" === "Pop_to_ind" || $"relationship" === "Ind_to_alt" || $"relationship" === "Ref_to_alt")
        val g3 = GraphFrame(subN3, subE3)

        t_st = System.nanoTime()
        val num_v_g3 = g3.vertices.cache.count
        t_ed = System.nanoTime()
        pw1.write("Number of nodes in sub graph g3: %d ".format(num_v_g3))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")

        t_st = System.nanoTime()
        val num_e_g3 = g3.edges.cache.count
        t_ed = System.nanoTime()
        pw1.write("Number of edges in sub graph g3: %d ".format(num_e_g3))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")


        t_st = System.nanoTime()
        val num_alts = g3.find("(a)-[ab]->(b); (b)-[bc]->(c); (d)-[dc]->(c)").filter("ab.relationship = 'Pop_to_ind'").filter("bc.relationship = 'Ind_to_alt'").filter("dc.relationship = 'Ref_to_alt'").count
        t_ed = System.nanoTime()
        pw1.write("Number of alt allels in sub graph g3 from EUR individuals: %d ".format(num_alts))
        pw1.write("\nQuery took " + (t_ed - t_st) / 1000000000 + " seconds\n")


        pw1.close()
        spark.stop()
        println("-------------------Done Job-------------------")
    }
}