package org.fedcentric.spark.thousandGenome

import java.io.{FileOutputStream, PrintWriter, FileNotFoundException, File}

import org.apache.hadoop.fs.Path
import org.apache.log4j.Level._
import org.apache.spark.SparkConf
import org.apache.spark.graphx._
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.fedcentric.spark.graph.{EnvironmentCreator, Logger}

/**
 * The main job is here, creating graph from VCF and text files. Parse and create graph then save into persistent storage
 */
object RunGraph extends EnvironmentCreator with Logger{

    /*
    val PATH_DATA = "/sandisk/caz25/annovar/genome1000/ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf"
    val PATH_ROOT = "hdfs://10.1.10.40:8020/VCFs"
    val PATH_DATA = "%s/ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf".format(PATH_ROOT)
    val TEST_PATH_DATA = "%s/%s,%s/%s".format(
        SourceDataPath,
        "ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf",
        SourceDataPath,
        "ALL.chr6.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf"
    )
    val filePath = "./src/main/resources/%s".format("thousandGenome/small3.txt")

    */

    var LogfileName:String = "/tmp/log.txt"
    // lazy val fileOutput=new PrintWriter(LogfileName:String)
    val fileOutput= new PrintWriter(new FileOutputStream(new File(LogfileName),true))

    def selflog(msg:String):Unit={
        fileOutput.print(msg)
        fileOutput.flush()
    }

    def SetSourceDataPath(path: String):String = {
        println("SourceDataPath:%s".format(path))
        path
    }

    def SetSavingPath(path: String):String = {
        println("TargetSavingPath:%s".format(path))
        path
    }
    /**
     *
     * @param args(0): path for source data
     *        args(1): path for storage
     */
    def main(args: Array[String]): Unit = {

        selflog("Beginning")
        println(args.length)
        args.length match{
            case 0 => log.fatal("No Source and Saving Paths defined"); System.exit(1)
            case 1 => log.fatal("No Saving Path defined"); System.exit(1)
            //case 2 => log.fatal("No Read Mode defined"); System.exit(1)
            case _ => println("Enough Args to run")
        }

        val SourceDataPath = SetSourceDataPath(args(0))
        val PATH_STORAGE = SetSavingPath(args(1))
        //val ReadMode = args(2)

        val conf_cluster = new SparkConf()
        conf_cluster.registerKryoClasses(Array(classOf[ParserVCF],
                                                classOf[GraphProducerVCF],
                                                classOf[StoreIntoParquetVCF],
                                                classOf[NodeDF],
                                                classOf[EdgeDF]))


        val spark = SparkSession.builder().config(conf_cluster).getOrCreate()
        log.setLevel(ALL)
        log.info("--Beginning Work")
        if(PATH_STORAGE.matches(".*(?i)hdfs.*")){
            log.info("--Choosing Data on HDFS")
            val res = new Path(PATH_STORAGE)
            val conf = spark.sparkContext.hadoopConfiguration
            val fs = org.apache.hadoop.fs.FileSystem.get(conf)
            try{
                if(!fs.exists(res)){
                    log.warn("Current Target directory does not exist, create then")
                    fs.mkdirs(res)
                }else{
                    log.info("--The path is valid")
                }
            }catch{
                case ex:FileNotFoundException => log.warn("file is not found")
            }finally{
                /**
                 * In client mode, it will print out this fatal info, yet will move forward
                 * Previously, even it has thrown something, the program will still move forward
                 * In cluster mode, it will stop at this point.
                 * May be because of permit issue
                 */
                log.fatal("HDFS file API has something wrong, exit")
                //System.exit(1)
            }
        }else{
            log.info("--Choosing Data on Local Disk")
            val res = new File(PATH_STORAGE)
            if(!res.exists()){
                log.warn("Current Target directory does not exist, create then")
                res.mkdir()
            }
        }

        log.info("--Start the work!")
        import spark.implicits._

        val filePath = SourceDataPath
        val readerVCF = new ReaderVCF(filePath, spark)
        val vcfFiles = readerVCF.LoadData()

        for(vcf<- vcfFiles) println(vcf)

        var isFirst = true
        var WallTime:Double = 0
        for(vcfFile <- vcfFiles){

            println("-----------------------------------------------------")
            println("Now is processing file %s".format(vcfFile)) //.split('/').last))
            println("-----------------------------------------------------")

            // recording how long to process each VCF file
            WallTime = time{
                val parserVCF = new ParserVCF(vcfFile, spark)
                val Data_Parsed_RDD = parserVCF.Parse()

                /**
                  * Mode will define whether using dataframe => graphframe or
                  * RDD=>GraphX
                  * 0: RDD[Node] and RDD[Edge] => GraphX[RDD[Node], RDD[EDGE]]
                  * 1: Dataframe to GraphFrame
                  */
                val mode = 1
                if(mode == 0) {
                    val graphProducerVCF = new GraphProducerVCF(Data_Parsed_RDD)
                    val NodeEdges = graphProducerVCF.CreateNodeEdgesGraphX()
                    val nodes = NodeEdges._1.map(x => (x._1, x._2))
                    val edges: RDD[org.apache.spark.graphx.Edge[EdgeParent]] = NodeEdges._2.map(x => org.apache.spark.graphx.Edge(x._1, x._2, x._3))
                    val graph: Graph[NodeParent, EdgeParent] = Graph(nodes, edges)

                }else if(mode == 1){
                    if(isFirst){
                        val NodeEdges_Json = GraphProducerVCF.CreateNodeEdgesFromJsonGraphFrame(spark)
                        println("Creating only once!!!!!!!!!!!!!!!!!11")
                        println("Creating nodes and edges which are from Json files")
                        val storeIntoParquetVCF_Node = new StoreIntoParquetVCF(NodeEdges_Json._1.toDF().coalesce(1))
                        storeIntoParquetVCF_Node.save("%s/Nodes/Node_%s".format(PATH_STORAGE, "JsonNodes_Ind_Pop_SuperPop"))
                        val storeIntoParquetVCF_Edge = new StoreIntoParquetVCF(NodeEdges_Json._2.toDF().coalesce(1))
                        storeIntoParquetVCF_Edge.save("%s/Edges/Edge_%s".format(PATH_STORAGE, "JsonEdges_Ind_Pop_SuperPop"))
                        isFirst = false
                    }
                    val graphProducerVCF = new GraphProducerVCF(Data_Parsed_RDD)
                    val NodeEdges = graphProducerVCF.CreateNodeEdgesGraphFrame()
                    val storeIntoParquetVCF_Node = new StoreIntoParquetVCF(NodeEdges._1.toDF())
                    storeIntoParquetVCF_Node.save("%s/Nodes/Node_%s".format(PATH_STORAGE, vcfFile.split('/').last))
                    val storeIntoParquetVCF_Edge = new StoreIntoParquetVCF(NodeEdges._2.toDF())
                    storeIntoParquetVCF_Edge.save("%s/Edges/Edge_%s".format(PATH_STORAGE, vcfFile.split('/').last))
                }else{
                    log.error("choosing the wrong mode, doing nothing")
                }
            }
            log.debug("Parsing and ingestion takes (%f) for file %s \n".format( WallTime, vcfFile.split('/').last))
        }

        spark.stop()
        selflog("Finished")

        println("-------------------Done Job-------------------")
    }
}



/*
for (node <- graph.vertices.collect()) {
    log.debug((node.toString() + "\n"))
}
for (edge <- graph.edges.collect()) {
    log(edge.toString + "\n")
}

// Simple query to find the neighbors of the first node of a given
// type and name
val query_machine = new QueryMachineVCF(graph)
val nodetype = "Alt_allele"
val name = "G"
val neighbors = query_machine.findNeighbors(nodetype, name)

log("A " + nodetype + " node " + name + " has the following neighbors: ")
for (a <- neighbors) {
    for (b <- a) {
        print(b._2.name + ", ")
    }
}

val t7: scala.Float = System.nanoTime()
log("\nQuery took " + (t7 - t6) / 1000000000 + " seconds.")
log("Total process time was " + (t7 - t0) / 1000000000 +
    " seconds.")
*/


/*
val PATH_ROOT = "hdfs://10.1.10.40:8020"
val PATH_DATA = "%s/VCFs/ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf".format(PATH_ROOT)
val t1 = sc.textFile(PATH_DATA)
val t2 = sc.textFile("/sandisk/caz25/annovar/genome1000/ALL.chr2.phase3_shapeit2_mvncall_integrated_v5a.20130502.genotypes.vcf")
t1.count
t2.count
*/